/* ----------------------------------------------------------------------------

 * GTSAM Copyright 2010, Georgia Tech Research Corporation, 
 * Atlanta, Georgia 30332-0415
 * All Rights Reserved
 * Authors: Frank Dellaert, et al. (see THANKS for the full author list)

 * See LICENSE for the license information

 * -------------------------------------------------------------------------- */

#include <stdio.h>
#include <CppUnitLite/TestHarness.h>

/* ************************************************************************* */
int main() { 
	TestResult tr; 
	int result = TestRegistry::runAllTests(tr);
	fprintf( stderr, "Press any key to close ..." ); getchar();
	return result;
}
/* ************************************************************************* */
