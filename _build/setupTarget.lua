function SetupTargetDir( dir, add_cfg, add_act )
	add_cfg = add_cfg or false;
	add_act = add_act or false;
	
	local act_sfx = "";
	if add_act then  act_sfx = "_" .. _ACTION; end
	
	configuration {}; targetdir( dir );
	configuration {"x64"}; targetdir( dir .. "64" .. act_sfx );
	configuration {"x32"}; targetdir( dir .. "32" .. act_sfx );

	if add_cfg then	
		configuration {"x64", "release"}; targetdir( dir .. "64"  .. act_sfx );
		configuration {"x64", "debug"};   targetdir( dir .. "64d" .. act_sfx );
		configuration {"x32", "release"};  targetdir( dir .. "32"  .. act_sfx );
		configuration {"x32", "debug"};   targetdir( dir .. "32d" .. act_sfx );
	end	
	configuration {}
end

function SetupTargetSfx( add_cfg, add_act, add_arch )
	if add_cfg==nil then add_cfg=true; end
	if add_act==nil then add_act=true; end
	add_arch = add_arch or false;
	
	local act_sfx = "";
	if add_act then  act_sfx = "_" .. _ACTION; end
	
	configuration {}; targetsuffix( act_sfx )
	
	if add_arch then 
		configuration {"x64"}; targetsuffix( "_64" .. act_sfx );
		configuration {"x32"}; targetsuffix( "_32" .. act_sfx );
	end	
	if add_cfg then
		configuration {"debug"}; targetsuffix( "_debug" .. act_sfx );
		configuration {"release"}; targetsuffix( "_release" .. act_sfx );
	end 	
	if add_arch and add_cfg then
		configuration {"x64", "release"};   targetsuffix( "_x64_release" .. act_sfx );
		configuration{"x64", "debug"};      targetsuffix( "_x64_debug" .. act_sfx );
		configuration {"x32", "release"};   targetsuffix( "_x32_release" .. act_sfx );
		configuration {"x32", "debug"};     targetsuffix( "_x32_debug" .. act_sfx );
	end	
	configuration {}
end
