#pragma once
#include <malloc.h>
#include <boost/static_assert.hpp>

namespace nstd {

	template< typename T , int Alignment > struct AlignedAllocator {
		static T* Allocate( int asize ) {
			size_t const sz = asize*sizeof(T);
			T* p = (T*)_aligned_malloc( sz,  Alignment );
			return p;
		}
		static void Deallocate( void* p) {
			_aligned_free( p );
		}
	
		static T* New( int asize ) {
			BOOST_STATIC_ASSERT( sizeof(T)%Alignment==0 );
			T* p = Allocate( asize );
			for( int i=0; i<asize; ++i  )
				new (p+i) T();
			return p;
		}
		static void Delete( T* p, int asize ) {
			BOOST_STATIC_ASSERT( sizeof(T)%Alignment==0 );
			for( int i=0; i<asize; ++i  )
				(p+i)->~T();
			Deallocate( p );
		}
	};


}