
function copyConfig() 
	if _ACTION and _ACTION ~= "clean" then
		local infile = io.open("config-default.h", "r")
		local text = infile:read("*a")

		--[[
		if _OPTIONS["no-trimesh"] then
		  text = string.gsub(text, "#define dTRIMESH_ENABLED 1", "/* #define dTRIMESH_ENABLED 1 */")
		  text = string.gsub(text, "#define dTRIMESH_OPCODE 1", "/* #define dTRIMESH_OPCODE 1 */")
		elseif (_OPTIONS["with-gimpact"]) then
		  text = string.gsub(text, "#define dTRIMESH_OPCODE 1", "#define dTRIMESH_GIMPACT 1")
		end

		if _OPTIONS["with-ou"] or not _OPTIONS["no-threading-intf"] then
		  text = string.gsub(text, "/%* #define dOU_ENABLED 1 %*/", "#define dOU_ENABLED 1")
		  text = string.gsub(text, "/%* #define dATOMICS_ENABLED 1 %*/", "#define dATOMICS_ENABLED 1")
		end

		if _OPTIONS["with-ou"] then
		  text = string.gsub(text, "/%* #define dTLS_ENABLED 1 %*/", "#define dTLS_ENABLED 1")
		end

		if _OPTIONS["no-threading-intf"] then
		  text = string.gsub(text, "/%* #define dTHREADING_INTF_DISABLED 1 %*/", "#define dTHREADING_INTF_DISABLED 1")
		elseif _OPTIONS["with-builtin-threading-impl"] then
		  text = string.gsub(text, "/%* #define dBUILTIN_THREADING_IMPL_ENABLED 1 %*/", "#define dBUILTIN_THREADING_IMPL_ENABLED 1")
		end

		if _OPTIONS["16bit-indices"] then
		  text = string.gsub(text, "#define dTRIMESH_16BIT_INDICES 0", "#define dTRIMESH_16BIT_INDICES 1")
		end
	  
		if _OPTIONS["old-trimesh"] then
		  text = string.gsub(text, "#define dTRIMESH_OPCODE_USE_OLD_TRIMESH_TRIMESH_COLLIDER 0", "#define dTRIMESH_OPCODE_USE_OLD_TRIMESH_TRIMESH_COLLIDER 1")
		end
		]]--
		local outfile = io.open("../gtsam/gtsam/config.h", "w")
		outfile:write(text)
		outfile:close()
	  end
end	  

function copyExport() 
	if _ACTION and _ACTION ~= "clean" then
		local infile = io.open("dllexport-default.h", "r")
		local text = infile:read("*a")

		--[[
		if _OPTIONS["no-trimesh"] then
		  text = string.gsub(text, "#define dTRIMESH_ENABLED 1", "/* #define dTRIMESH_ENABLED 1 */")
		  text = string.gsub(text, "#define dTRIMESH_OPCODE 1", "/* #define dTRIMESH_OPCODE 1 */")
		elseif (_OPTIONS["with-gimpact"]) then
		  text = string.gsub(text, "#define dTRIMESH_OPCODE 1", "#define dTRIMESH_GIMPACT 1")
		end

		if _OPTIONS["with-ou"] or not _OPTIONS["no-threading-intf"] then
		  text = string.gsub(text, "/%* #define dOU_ENABLED 1 %*/", "#define dOU_ENABLED 1")
		  text = string.gsub(text, "/%* #define dATOMICS_ENABLED 1 %*/", "#define dATOMICS_ENABLED 1")
		end

		if _OPTIONS["with-ou"] then
		  text = string.gsub(text, "/%* #define dTLS_ENABLED 1 %*/", "#define dTLS_ENABLED 1")
		end

		if _OPTIONS["no-threading-intf"] then
		  text = string.gsub(text, "/%* #define dTHREADING_INTF_DISABLED 1 %*/", "#define dTHREADING_INTF_DISABLED 1")
		elseif _OPTIONS["with-builtin-threading-impl"] then
		  text = string.gsub(text, "/%* #define dBUILTIN_THREADING_IMPL_ENABLED 1 %*/", "#define dBUILTIN_THREADING_IMPL_ENABLED 1")
		end

		if _OPTIONS["16bit-indices"] then
		  text = string.gsub(text, "#define dTRIMESH_16BIT_INDICES 0", "#define dTRIMESH_16BIT_INDICES 1")
		end
	  
		if _OPTIONS["old-trimesh"] then
		  text = string.gsub(text, "#define dTRIMESH_OPCODE_USE_OLD_TRIMESH_TRIMESH_COLLIDER 0", "#define dTRIMESH_OPCODE_USE_OLD_TRIMESH_TRIMESH_COLLIDER 1")
		end
		]]--
		local outfile = io.open("../gtsam/gtsam/dllexport.h", "w")
		outfile:write(text)
		outfile:close()
	  end
end

