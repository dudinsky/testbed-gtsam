/* ----------------------------------------------------------------------------

 * GTSAM Copyright 2010, Georgia Tech Research Corporation,
 * Atlanta, Georgia 30332-0415
 * All Rights Reserved
 * Authors: Frank Dellaert, et al. (see THANKS for the full author list)

 * See LICENSE for the license information

 * -------------------------------------------------------------------------- */

/**
 *  @file  testSmartProjectionPoseFactor.cpp
 *  @brief Unit tests for ProjectionFactor Class
 *  @author Chris Beall
 *  @author Luca Carlone
 *  @author Zsolt Kira
 *  @author Frank Dellaert
 *  @date   Sept 2013
 */

#include <gtsam/slam/SmartProjectionPoseFactor.h>
#include <gtsam/slam/SmartProjectionFactor.h>
#include <gtsam/slam/GeneralSFMFactor.h>
#include <gtsam/geometry/Cal3_S2.h>
#include <gtsam/geometry/Cal3Bundler.h>

//#include "smartFactorScenarios.h"

#include <gtsam/slam/ProjectionFactor.h>
#include <gtsam/slam/PoseTranslationPrior.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/base/numericalDerivative.h>
//#include <gtsam/base/serializationTestHelpers.h>
#include <CppUnitLite/TestHarness.h>
//#include <boost/assign/std/map.hpp>
//#include <iostream>

//using namespace boost::assign;

// Make more verbose like so (in tests):
// params.verbosityLM = LevenbergMarquardtParams::SUMMARY;
using std::vector;

namespace gtsam {

	template<class CAMERA>
	void projectToMultipleCameras( const CAMERA& cam1, const CAMERA& cam2,
		const CAMERA& cam3, Point3 landmark, vector<Point2>& measurements_cam ) {
		Point2 cam1_uv1 = cam1.project( landmark );
		Point2 cam2_uv1 = cam2.project( landmark );
		Point2 cam3_uv1 = cam3.project( landmark );
		measurements_cam.push_back( cam1_uv1 );
		measurements_cam.push_back( cam2_uv1 );
		measurements_cam.push_back( cam3_uv1 );
	}
}


/* *************************************************************************/
TEST( SmartProjectionPoseFactor, 3poses_rotation_only_smart_projection_factor ) {
	using namespace gtsam;
	// Convenience for named keys
	using symbol_shorthand::X;
	using symbol_shorthand::L;

	const double rankTol = 1.0;
	// Create a noise model for the pixel error
	const double sigma = 0.1;
	SharedIsotropic model( noiseModel::Isotropic::Sigma( 2, sigma ) );

	// tests data
	Symbol x1( 'X', 1 );
	Symbol x2( 'X', 2 );
	Symbol x3( 'X', 3 );

	Point2 measurement1( 323.0, 240.0 );

	LevenbergMarquardtParams lmParams;
	double fov = 60; // degrees
	int w = 640, h = 480;
	Point3 landmark1( 5, 0.5, 1.2 );
	Point3 landmark2( 5, -0.5, 1.2 );
	Point3 landmark3( 3, 0, 3.0 );
	Point3 landmark4( 10, 0.5, 1.2 );
	Point3 landmark5( 10, -0.5, 1.2 );

	// First camera pose, looking along X-axis, 1 meter above ground plane (x-y)
	Pose3 level_pose = Pose3( Rot3::Ypr( -M_PI / 2, 0., -M_PI / 2 ), Point3( 0, 0, 1 ) );
	// Second camera 1 meter to the right of first camera
	Pose3 pose_right = level_pose * Pose3( Rot3(), Point3( 1, 0, 0 ) );
	// Third camera 1 meter above the first camera
	Pose3 pose_above = level_pose * Pose3( Rot3(), Point3( 0, -1, 0 ) );

  //using namespace vanillaPose;
	typedef PinholePose<Cal3_S2> Camera;
	typedef SmartProjectionPoseFactor<Cal3_S2> SmartFactor;
	Cal3_S2::shared_ptr sharedK( new Cal3_S2( fov, w, h ) );
	Camera level_camera( level_pose, sharedK );
	Camera level_camera_right( pose_right, sharedK );
	Camera cam1( level_pose, sharedK );
	//Camera cam2( pose_right, sharedK );
	//Camera cam3( pose_above, sharedK );

	vector<Key> views;
	views.push_back(x1);
	views.push_back(x2);
	views.push_back(x3);

	// Two different cameras, at the same position, but different rotations
	Pose3 pose2 = level_pose* Pose3(Rot3::RzRyRx(-0.05, 0.0, -0.05), Point3(0, 0, 0));
	Pose3 pose3 = pose2 * Pose3(Rot3::RzRyRx(-0.05, 0.0, -0.05), Point3(0, 0, 0));
	Camera cam2(pose2, sharedK);
	Camera cam3(pose3, sharedK);

	vector<Point2> measurements_cam1, measurements_cam2, measurements_cam3;

	// Project three landmarks into three cameras
	projectToMultipleCameras(cam1, cam2, cam3, landmark1, measurements_cam1);
	projectToMultipleCameras(cam1, cam2, cam3, landmark2, measurements_cam2);
	projectToMultipleCameras(cam1, cam2, cam3, landmark3, measurements_cam3);

	SmartProjectionParams params;
	params.setRankTolerance(10);
	params.setDegeneracyMode(gtsam::ZERO_ON_DEGENERACY);

	SmartFactor::shared_ptr smartFactor1(
		new SmartFactor(model, sharedK, boost::none, params));
	smartFactor1->add(measurements_cam1, views);

	SmartFactor::shared_ptr smartFactor2(
		new SmartFactor(model, sharedK, boost::none, params));
	smartFactor2->add(measurements_cam2, views);

	SmartFactor::shared_ptr smartFactor3(
		new SmartFactor(model, sharedK, boost::none, params));
	smartFactor3->add(measurements_cam3, views);

	const SharedDiagonal noisePrior = noiseModel::Isotropic::Sigma(6, 0.10);
	const SharedDiagonal noisePriorTranslation = noiseModel::Isotropic::Sigma(3,0.10);
	Point3 positionPrior = Point3(0, 0, 1);

	NonlinearFactorGraph graph;
	graph.push_back(smartFactor1);
	graph.push_back(smartFactor2);
	graph.push_back(smartFactor3);
	graph.push_back(PriorFactor<Pose3>(x1, cam1.pose(), noisePrior));
	graph.push_back(PoseTranslationPrior<Pose3>(x2, positionPrior, noisePriorTranslation));
	graph.push_back(PoseTranslationPrior<Pose3>(x3, positionPrior, noisePriorTranslation));

	//Pose3 noise_pose = Pose3(Rot3::Ypr(-M_PI/10, 0., -M_PI/10), Point3(0.5,0.1,0.3)); // noise from regular projection factor test below
	Pose3 noise_pose = Pose3(Rot3::Ypr(-M_PI / 100, 0., -M_PI / 100), Point3(0.1, 0.1, 0.1)); // smaller noise
	Values values;
	values.insert(x1, cam1.pose());
	values.insert(x2, cam2.pose());
	values.insert(x3, pose3/* noise_pose*/);

	EXPECT(
		assert_equal(
			Pose3(
				Rot3(0.00563056869, -0.130848107,  0.991386438, 
				    -0.991390265,   -0.130426831, -0.0115837907, 
					 0.130819108,   -0.98278564,  -0.130455917),
				Point3(0.0897734171, -0.110201006, 0.901022872)),
			values.at<Pose3>(x3)));

	Values result;
	LevenbergMarquardtOptimizer optimizer(graph, values, lmParams);
	result = optimizer.optimize();

	// Since we do not do anything on degenerate instances (ZERO_ON_DEGENERACY)
	// rotation remains the same as the initial guess, but position is fixed by PoseTranslationPrior
	EXPECT(assert_equal(Pose3(values.at<Pose3>(x3).rotation(),
		Point3(0,0,1)), result.at<Pose3>(x3)));
}



/* ************************************************************************* */
int main() {
  TestResult tr;
  int res=TestRegistry::runAllTests(tr);
  fprintf( stderr, "Press any key to close ..." ); getchar();
  return res;	
}
/* ************************************************************************* */

