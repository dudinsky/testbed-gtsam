-- print "uncomment this hello premake4 world for debugging the script"
pathToRoot = ".."
pathToSdks = pathToRoot.."/../3rdparty"
pathToBoost = pathToSdks.."/boost_1_57_0"

dofile "copyConfig.lua"
dofile "setupTarget.lua"

--copyConfig();
--copyExport();

solution "_GTSAM"
	platforms {"x32", "x64"}
	configurations { "Release", "Debug" }
	configuration "Release*"
		flags { "Symbols", "Optimize", "NoMinimalRebuild", }
		defines { "NDEBUG","NOMINMAX"}
	configuration "Debug*"
		flags { "Symbols", "NoMinimalRebuild" ,"NoEditAndContinue" }
		defines { "_DEBUG", "NOMINMAX"}
	configuration {}
		defines { "WIN32"}
	configuration {"x64"} 
		defines { "WIN64"}
	configuration {"Windows"}
		defines { "WINDOWS", "_WINDOWS", "WIN32", "_WIN32", "_CRT_SECURE_NO_WARNINGS","_CRT_SECURE_NO_DEPRECATE", "_SCL_SECURE_NO_WARNINGS" }
	configuration{}
	

	--defines { "CVD_HAVE_XMMINTRIN" }	
	--defines { "PTW32_STATIC_LIB", "GLEW_STATIC", "LIB3DSAPI=" }
	--defines { "HAVE_CVCONFIG_H" }
	defines { "BOOST_ALL_NO_LIB" }
	--defines { "EIGEN_DONT_ALIGN_STATICALLY" }
	defines { "BOOST_HAS_DECLSPEC", "BOOST_SYMBOL_EXPORT", "BOOST_SYMBOL_IMPORT" }
	
	
	
	buildoptions { "/MP"  } -- Multithreaded compiling
	flags { "NoIncrementalLink" } 
	--flags { "SymbolsToObjs" } 
	--flags { "StaticRuntime" } 
	--flags { "NoRTTI"}	
	--flags { "ImportLibToObjs" }	
	
	act = _ACTION
	projdir = "./" .. _ACTION
	language "C++"; location( projdir ); --SetupTargetDir( "bin" ); SetupTargetSfx();
	
	--startproject "PTAM"

	group "3rdParties"
	
		project( "CppUnitLite" )
			language "C++"; location( projdir ); kind "StaticLib"; SetupTargetDir( _ACTION.."/libs" ); SetupTargetSfx();
			includedirs { pathToBoost }
			files { pathToSdks.."/CppUnitLite/*.cpp", pathToSdks.."/CppUnitLite/*.h" }
			
		project( "boost" )
			language "C++"; location( projdir ); kind "StaticLib"; SetupTargetDir( _ACTION.."/libs" ); SetupTargetSfx();
			includedirs { pathToBoost }
			defines { "BOOST_THREAD_BUILD_LIB" }
			files { pathToBoost.."/libs/system/src/**.cpp" }
			files { pathToBoost.."/libs/filesystem/src/**.cpp" }
			files { pathToBoost.."/libs/thread/src/*.cpp" }
			files { pathToBoost.."/libs/thread/src/win32/*.cpp" }
			files { pathToBoost.."/libs/serialization/src/*.cpp" }
			files { pathToBoost.."/libs/timer/src/*.cpp" }
			files { pathToBoost.."/libs/chrono/src/*.cpp" }
			files { pathToBoost.."/libs/program_options/src/*.cpp" }

	local pathToGtsam = pathToRoot.."/gtsam"
	local extraConsistencyChecks = true;

    function gtsamAddInclude()
		    includedirs { "./" }
			includedirs { pathToSdks, pathToSdks.."/include", pathToBoost }
			includedirs { pathToGtsam }
			includedirs { pathToGtsam.."/gtsam/3rdparty/GeographicLib/include" }
			defines { "GTSAM_IMPORT_STATIC", "USE_GKREGEX" }
			if( extraConsistencyChecks ) then
				defines { "GTSAM_EXTRA_CONSISTENCY_CHECKS" }
			end
			defines { "__thread=__declspec( thread )" }
    end	
	function gtsamAddPrecompiled()
			local pchs = "precomp.cpp";
			local pchh = "precomp.h"; --"./gtsam/precomp.h"
			pchheader( pchh );  
			pchsource( pchs ); files {  pchs };  
			buildoptions { '/FI "'..pchh..'"' } 
			buildoptions { "/Zm128"  }
	end	
	
	
	group "gtsam"	
		project( "gtsam_core" )	
			language "C++"; location( projdir ); kind "StaticLib"; SetupTargetDir( _ACTION.."/libs" ); SetupTargetSfx();
			--objdir "!obj/%{cfg.platform}_%{cfg.buildcfg}"
			--flags { "OptimizeSpeed" };
			--flags { "FloatFast" }
			
			gtsamAddInclude();
			
			includedirs { pathToGtsam.."/gtsam/3rdparty/UFconfig" }
			includedirs { pathToGtsam.."/gtsam/3rdparty/CCOLAMD/include" }
			includedirs { pathToGtsam.."/gtsam/3rdparty/metis/include" }
			includedirs { pathToGtsam.."/gtsam/3rdparty/metis/libmetis" }
			includedirs { pathToGtsam.."/gtsam/3rdparty/metis/GKLib" }
			
			local mp_ = pathToGtsam.."/gtsam/";
			
			files { mp_.."base/*.cpp", mp_.."base/*.h", mp_.."inference/*.cpp", mp_.."inference/*.h" }
			files { mp_.."discrete/*.cpp", mp_.."discrete/*.h", mp_.."geometry/*.cpp", mp_.."geometry/*.h" }
			files { mp_.."linear/*.cpp", mp_.."linear/*.h", mp_.."navigation/*.cpp", mp_.."navigation/*.h" } 
			files { mp_.."nonlinear/*.cpp", mp_.."nonlinear/*.h" }
			files { mp_.."sam/*.cpp", mp_.."sam/*.h", mp_.."sfm/*.cpp", mp_.."sfm/*.h" } 
			files { mp_.."slam/*.cpp", mp_.."slam/*.h", mp_.."sfm/*.cpp", mp_.."sfm/*.h" } 
			files { mp_.."symbolic/*.cpp", mp_.."symbolic/*.h" }
			
			files { mp_.."3rdparty/GeographicLib/src/*.cpp" }
		
			files { mp_.."3rdparty/CCOLAMD/Source/*.c" }
			files { mp_.."3rdparty/metis/libmetis/*.c" }
			files { mp_.."3rdparty/metis/GKLib/*.c" }
			
			gtsamAddPrecompiled();
			
			buildoptions { "/wd4101" }
			configuration { mp_.."3rdparty/CCOLAMD/Source/*.c" }
				flags { "NoPCH" }
			configuration { mp_.."3rdparty/metis/libmetis/*.c" }
				flags { "NoPCH" }
				buildoptions { "/wd4005",  "/wd4018", "/wd4244" }
			configuration { mp_.."3rdparty/metis/GKLib/*.c" }
				flags { "NoPCH" }
				buildoptions { "/wd4005",  "/wd4018", "/wd4244" }
				
			
	    group "gtsam-tests"	
		
		function gtsamTestProject( name,  multiTest, ... )
			project( name )
			kind "ConsoleApp"; language "C++"; location( projdir ); SetupTargetSfx(); --SetupTargetDir( "bin" ); --debugdir "$(TargetDir)";
			configuration {}; targetdir( "../bin" ); debugdir(".."); --debugdir("../../3rdparty");
			--objdir "!obj/%{cfg.platform}_%{cfg.buildcfg}"
			
			gtsamAddInclude();
			gtsamAddPrecompiled();			
			
			links { "gtsam_core", "CppUnitLite", "boost" }
			
			for i,path in ipairs(arg) do
				files { path };
			end
			
			if( multiTest ) then
			
				files { "run-tests.cpp" }	
				for i,path in ipairs(arg) do
					configuration { path }
					defines { "main=inline no_main" }
				end
			end
			
				
			--configuration { mp_.."test*.s11n.cpp" }
				--buildoptions { "/bigobj" }
			--configuration { mp_.."test*Serialization.cpp" }
				--buildoptions { "/bigobj" }
				
			configuration {} 
			buildoptions { "/wd4101" }			
			configuration {"x64"} 
			buildoptions { "/bigobj" }			
			configuration {}
		end
		--gtsamTestProject( "0-test",  false,  pathToGtsam.."/tests/testPCGSolver.cpp" );
		--gtsamTestProject( "0-test",  false,  pathToGtsam.."/gtsam/inference/tests/testOrdering.cpp" );
		gtsamTestProject( "0-test",  false,  "testSmartProjectionPoseFactor1.cpp" );
		gtsamTestProject( "1-core",  true,  pathToGtsam.."/tests/test*.cpp" );
		gtsamTestProject( "2-base",  true,  pathToGtsam.."/gtsam/base/tests/test*.cpp" );
		gtsamTestProject( "3-discrete",  true,  pathToGtsam.."/gtsam/discrete/tests/test*.cpp" );
		gtsamTestProject( "4-inference",  true,  pathToGtsam.."/gtsam/inference/tests/test*.cpp" );
		gtsamTestProject( "5-navigation",  true,  pathToGtsam.."/gtsam/navigation/tests/test*.cpp" );
			includedirs { pathToGtsam.."/gtsam/3rdparty/GeographicLib/include" }
		gtsamTestProject( "6-nonlinear",  true,  pathToGtsam.."/gtsam/nonlinear/tests/test*.cpp" );
		gtsamTestProject( "7-sam",  true,  pathToGtsam.."/gtsam/sam/tests/test*.cpp" );
		gtsamTestProject( "8-slam",  true,  pathToGtsam.."/gtsam/slam/tests/test*.cpp" );

		--[[	
		project( "gtsam_unstable" )	
			language "C++"; location( projdir ); kind "StaticLib"; SetupTargetDir( _ACTION.."/libs" ); SetupTargetSfx();
			flags { "OptimizeSpeed" };
			flags { "FloatFast" }
			defines { "GTSAM_UNSTABLE_IMPORT_STATIC" }

			includedirs { pathToSdks, pathToSdks.."/include", pathToBoost }
			includedirs { pathToSdks.."/gtsam" }
			--includedirs { pathToSdks.."/gtsam/gtsam/3rdparty/UFconfig" }
			
			local mp_ = pathToSdks.."/gtsam/gtsam_unstable/";
			files { mp_.."base/*.cpp", mp_.."base/*.h" }
			files { mp_.."discrete/*.cpp", mp_.."discrete/*.h" }
			files { mp_.."dynamics/*.cpp", mp_.."dynamics/*.h" }
			files { mp_.."geometry/*.cpp" }
			files { mp_.."linear/*.cpp" }
			files { mp_.."nonlinear/*.cpp" }
			files { mp_.."partition/*.cpp" }
			files { mp_.."slam/*.cpp" }
			--files { mp_.."timing/*.cpp" }

			local pch_ = pathToSdks.."/gtsam/gtsam/precomp.cpp";
			pchheader( "gtsam/precomp.h" );  pchsource( pch_ ); files {  pch_ };  buildoptions { '/FI "gtsam/precomp.h"' } --buildoptions { "/Zm265"  }
			
			buildoptions { "/wd4101" }
		--]]	
			
		
		--[[
		project( "gtsam_tests" )
			kind "ConsoleApp"; language "C++"; location( projdir ); SetupTargetSfx(); --SetupTargetDir( "bin" ); --debugdir "$(TargetDir)";
			configuration {}; targetdir( "../bin" ); debugdir "../bin";
			includedirs { pathToBoost, pathToInclude, pathToSdks.."/gtsam" }
			includedirs { pathToSdks.."/gtsam/gtsam/3rdparty/GeographicLib/include" }
			defines { "GTSAM_IMPORT_STATIC" }
			
			links { "gtsam_core", "CppUnitLite", "boost" }
			local mp_ = pathToSdks.."/gtsam/gtsam/base/tests/";  files { mp_.."test*.cpp" }

			mp_ = pathToSdks.."/gtsam/gtsam/discrete/tests/";	     files { mp_.."test*.cpp" }
			mp_ = pathToSdks.."/gtsam/gtsam/geometry/tests/";	 files { mp_.."test*.cpp" }
			mp_ = pathToSdks.."/gtsam/gtsam/inference/tests/";	 files { mp_.."test*.cpp" }
			mp_ = pathToSdks.."/gtsam/gtsam/linear/tests/";	     files { mp_.."test*.cpp" }
			mp_ = pathToSdks.."/gtsam/gtsam/navigation/tests/";	 files { mp_.."test*.cpp" }
			mp_ = pathToSdks.."/gtsam/gtsam/nonlinear/tests/";	 files { mp_.."test*.cpp" }
			mp_ = pathToSdks.."/gtsam/gtsam/sam/tests/";	         files { mp_.."test*.cpp" }
			mp_ = pathToSdks.."/gtsam/gtsam/slam/tests/";	         files { mp_.."test*.cpp" }
			mp_ = pathToSdks.."/gtsam/gtsam/symbolic/tests/";	 files { mp_.."test*.cpp" }
			
			files { pathToSdks.."/gtsam/gtsam/run-tests.cpp" }	
			
			local pch_ = pathToSdks.."/gtsam/gtsam/precomp.cpp";
			pchheader( "gtsam/precomp.h" );  pchsource( pch_ ); files {  pch_ };  buildoptions { '/FI "gtsam/precomp.h"' } buildoptions { "/Zm265"  }
		
			configuration { pathToSdks.."/gtsam/gtsam/*/tests/test*.cpp" }
				defines { "main=inline no_main" }
		--]]
		--[[		
		project( "gtsam_test" )	
		kind "ConsoleApp"; language "C++"; location( projdir ); SetupTargetSfx(); --SetupTargetDir( "bin" ); --debugdir "$(TargetDir)";
			configuration {}; targetdir( "../bin" ); debugdir "../bin";
			includedirs { pathToBoost, pathToInclude }
			includedirs { pathToSdks.."/gtsam" }
			links { "gtsam_core", "CppUnitLite", "boost" }
			mp_ = pathToSdks.."/gtsam/gtsam/linear/tests/";	
			files { mp_.."testErrors.cpp" }
		--]]
			
			
	group "gtsam-examples"
		local pathToExamples = pathToGtsam.."/examples"
		function gtsamExampleProject( path, name )			
			project( name )
			defines { "GTSAM_IMPORT_STATIC" }
			kind "ConsoleApp"; language "C++"; location( projdir ); SetupTargetSfx(); --SetupTargetDir( "bin" ); --debugdir "$(TargetDir)";
			configuration {}; targetdir( "../bin" ); debugdir(".."); --debugdir "../bin";
			gtsamAddInclude();
			links { "gtsam_core",  "boost" }
			files { path.."/"..name..".cpp" }
			buildoptions { "/wd4101" }			
		end
		gtsamExampleProject( pathToExamples, "CameraResectioning" );
		gtsamExampleProject( pathToExamples, "CreateSFMExampleData" ); 
		--gtsamExampleProject( pathToExamples, "DiscreteBayesNet_FG" );
		
		gtsamExampleProject( pathToExamples, "easyPoint2KalmanFilter" );
		--gtsamExampleProject( pathToExamples, "elaboratePoint2KalmanFilter" );
		gtsamExampleProject( pathToExamples, "LocalizationExample" );
		gtsamExampleProject( pathToExamples, "METISOrderingExample" );
		gtsamExampleProject( pathToExamples, "OdometryExample" );
		gtsamExampleProject( pathToExamples, "PlanarSLAMExample" );
		gtsamExampleProject( pathToExamples, "Pose2SLAMExample" ); 
		gtsamExampleProject( pathToExamples, "Pose2SLAMExample_g2o" ); 
		gtsamExampleProject( pathToExamples, "Pose2SLAMExample_graph" ); 
		gtsamExampleProject( pathToExamples, "Pose2SLAMExample_graphviz" ); 
		gtsamExampleProject( pathToExamples, "Pose2SLAMExample_lago" ); 
		gtsamExampleProject( pathToExamples, "Pose2SLAMExampleExpressions" );
		gtsamExampleProject( pathToExamples, "Pose2SLAMwSPCG" );
		gtsamExampleProject( pathToExamples, "Pose3SLAMExample_changeKeys" );
		gtsamExampleProject( pathToExamples, "Pose3SLAMExample_g2o" );
		gtsamExampleProject( pathToExamples, "Pose3SLAMExample_initializePose3Chordal" );
		gtsamExampleProject( pathToExamples, "Pose3SLAMExample_initializePose3Gradient" );
		gtsamExampleProject( pathToExamples, "RangeISAMExample_plaza2" );
		gtsamExampleProject( pathToExamples, "SelfCalibrationExample" ); 
		gtsamExampleProject( pathToExamples, "SFMExample" ); 
		gtsamExampleProject( pathToExamples, "SFMExample_bal" );
		gtsamExampleProject( pathToExamples, "SFMExample_bal_COLAMD_METIS" );
		gtsamExampleProject( pathToExamples, "SFMExample_SmartFactor" );
		gtsamExampleProject( pathToExamples, "SFMExample_SmartFactorPCG" );
		gtsamExampleProject( pathToExamples, "SFMExampleExpressions" );
		gtsamExampleProject( pathToExamples, "SFMExampleExpressions_bal" );
		gtsamExampleProject( pathToExamples, "SimpleRotation" ); 
		gtsamExampleProject( pathToExamples, "SolverComparer" );
		gtsamExampleProject( pathToExamples, "StereoVOExample" );
		gtsamExampleProject( pathToExamples, "StereoVOExample_large" );
		gtsamExampleProject( pathToExamples, "TimeTBB" ); 
		--gtsamExampleProject( pathToExamples, "UGM_chain" );
		--gtsamExampleProject( pathToExamples, "UGM_small" );
		gtsamExampleProject( pathToExamples, "VisualISAM2Example" );
		gtsamExampleProject( pathToExamples, "VisualISAMExample" );
		
		
			
			
		--[[		
		project "PTAMM"
			defines { "MEASUREMENT_HAS_CAMDERIVS=1" }
			--defines { "MapMaker_UNPROJECT_CACHE=1" }
			defines { "ENABLE_MODELS_GAME=1" }
			kind "ConsoleApp"; language "C++"; location( projdir ); SetupTargetSfx(); --SetupTargetDir( "bin" ); --debugdir "$(TargetDir)";
			configuration {}; targetdir( "../bin" ); debugdir "../bin";
			
			includedirs { pathToBoost, pathToInclude, pathToLibCvd, pathToTooN }
			includedirs { pathToRoot.."/ptamm", pathToRoot.."/common", pathToSdks.."/gvars-3.0", pathToSdks.."/pthreads.2" }
			links { "libpng", "libjpeg", "zlib", "gvars3", "libcvd+TooN", "clapack", "pthread-win32", "agast" }
			includedirs { pathToSdks.."/lib3ds/src" }  links { "lib3ds" }
			includedirs { pathToSdks.."/agast/include" }
	
			LinkOpenGL(1,1)
		
			local path_ = pathToRoot.."/ptamm/"
			files { path_.."*.cc", path_.."*.h", pathToRoot.."/common/*.cc", pathToRoot.."/common/*.h" }
			
			--files { pathToRoot.."/common/VideoSources/VideoSource_Win32_EWCLIB.cc" }
			files { pathToRoot.."/common/VideoSources/VideoSource_Linux_OpenCV.cc" } LinkOpenCV()
			
			pchheader( "precompiled.h" );
			pchsource( pathToRoot.."/common/precompiled.cc" ) 
		--]]